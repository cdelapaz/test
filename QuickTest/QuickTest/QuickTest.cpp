// QuickTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	int num = -1;
	cout << "Enter a number: _____\b\b\b\b";
	while (true)
	{
		cin >> noskipws >> num;
		if (cin.bad() || num < 0 || num > 100)
		{
			cout << "Invalid Number\n Enter a new Number: _____\b\b\b\b";
			cin.clear();
			cin.ignore(INT_MAX, '\n');
		}
		else
		{
			break;
		}	
	}

	system("cls");

	cout << "Your number is " << num << endl;

	system("pause");
	return 0;
}

